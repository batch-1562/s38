// console.log('Hello fom JS');

// const firstName = document.querySelector('#firstName');
// const lastName = document.querySelector('#lastName');

// [SECTION] getElementById => used when targeting a single component
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');

// [SECTION] getElementById => used when targeting multiple comp at the same time
const inputFields = document.getElementsByClassName('form-control');


// [SECTION] getElementById =>
const heading = document.getElementsByTagName('h3');

console.log(firstName);
console.log(lastName);
console.log(inputFields);
console.log(heading);